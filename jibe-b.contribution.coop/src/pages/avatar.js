import React, { useState } from "react"

export default () => {

    const [timeWokeUp, setTimeWokeUp] = useState("0:00")
    return(
        <div>
            Durée éveillé : 10 heures (lever : {timeWokeUp})

            <div>
                Taux de sucre, Graphique d'évolution, projection, 
            </div>
            <div>
                Suggestion : notification "time to…"
            </div>
            <div>
                Entrée de log : viens de faire telle action (airtable)
            </div>
            <div>
                Feedback bar : Cette suggestion était [trop tôt] [trop tard] 
            </div>

            Tâches en cours
            <div>
                <iframe title="framagenda-todo" src="https://framagenda.org/apps/calendar/" width="100%" height="640px" ></iframe>
            </div>

            Agenda
            <div>
                <iframe title="framagenda-agenda" src="https://framagenda.org/apps/calendar/" width="100%" height="640px" ></iframe>
            </div>

            Administratif
            <div>
                <a src="https://app.expensya.com/Portal/#/Dashboard">Expensya (frais indirects)</a>
            </div>
        </div>
    )
}