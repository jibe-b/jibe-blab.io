import React from "react"

//import { Fragment } from 'react';

import Md from "../components/basic_html_from_markdown"

//import Media from "react-media"
import { useMediaPredicate } from "react-media-hook";

import Layout from "../components/layout"

export default () => {

  1 < 2 && console.log(true)
  /* 
  {
    (1 < 2 && "A") || "B"
  }
   */

  var projects = [ // /!\ pas de tirets possibles dans le nom du projet à cause url shields.io
    {
        "name": "Cloud coopératif https://pad.lamyne.org/_bs9umppSgGQltAUbx8-rA#",
        "shield": "pass",
        "how_to_help": "https://example.com"
    },
    {
      "name": "Consortium coopératif d'économie réelle militante https://pad.lamyne.org/5RNAVWRvQgGHimPtXYEVVg",
      "shield": "pass",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Mise en place de la contribution chez Coopaname https://pad.lamyne.org/WK7gFL_3QMiUWDsD7A8TVw",
      "shield": "pass",
      "how_to_help": "https://example.com"
    },
    {
      "name": "cantine populaire décentralisée",
      "shield": "pass",
      "how_to_help": "https://example.com"
    },
    {
      "name": "apéros écocolocations partout en France",
      "shield": "pass",
      "how_to_help": "https://example.com"
    },
    {
      "name": "ateliers écocolocations partout en France",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Gamme meubles Strasbourg",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Gamme vêtements Strasbourg",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Exode réseaux sociaux https://www.facebook.com/groups/2290304727921896/",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Monde du jour et de la nuit (festivals éco-responsables) https://www.facebook.com/groups/2450611008574230/",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Ces dossiers #InstructionCollaborative de dossiers #DémocratieLocale",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Urbanisme participatif",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Le Macroscope",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "HackYourResearch Strasbourg - OpenData, OpenTout",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Free Open Source Squad",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Traductions posts acteurs du changement",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Gamme vêtements Strasbourg",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Coopétitions ScénarioB",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Ta Commande",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Cagnotte Conciergerie - rétribution de la contribution",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Mégaphone des initiatives citoyennes",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Végétalisons",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "RSSquat https://gitlab.com/jibe-b/rssquater",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Design partout https://pad.lamyne.org/WD1G4i2eQmaek4SLFx7Hcw",
      "shield": "fail",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Animation de communautés Alternative Rail https://www.facebook.com/groups/2163956663833344/ Velotaf https://www.facebook.com/groups/2069942019734778/ Eco-colocation https://www.facebook.com/groups/470511086490229/",
      "shield": "pass",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Curation participative https://www.facebook.com/groups/2406321719664232",
      "shield": "pass",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Free&OpenSource Software and Data Analysis livecoding https://www.facebook.com/groups/2662035420792463/",
      "shield": "pass",
      "how_to_help": "https://example.com"
    },
    {
      "name": "Curation #NawakDetected",
      "shield": "pass",
      "how_to_help": "https://example.com"
    },
    {
      "name": "elle est où l'entourloupe ? Journalsime d'investigation participatif et méga sourcé https://www.facebook.com/groups/2820645064927945",
      "shield": "pass",
      "how_to_help": "https://example.com"
    },
    {
      "name": "instruction de dossiers pour des coopératives",
      "infos": "Commown, Mobicoop, RailCoop, TeleCoop, La Coop des Masques, Société des Consommateur·ices, Coopaname, StartinBlox, Mobicoop, LabelEmmaüs, [SC Bastia, La Nef, Enercoop, l'AtelierPaysan, Smart, CoopCycle]"
    },
    {
      "name": "Macroscope #Pluviométrie",
      "infos": ""
    }

    
    
  ]


  return (
    <Layout>
      {/* 
      <div>
        <Media queries={{
          small: "(max-width: 599px)",
          medium: "(min-width: 600px) and (max-width: 1199px)",
          large: "(min-width: 1200px)"
        }}>
          {matches => (
            <Fragment>
              {matches.small && <p>I am small!</p>}
              {matches.medium && <p>I am medium!</p>}
              {matches.large && <p>I am large!</p>}
            </Fragment>
          )}
        </Media>
      </div>
*/}



      <div title="projects">
        {
          projects.map(
            
            item => {
              const color = item.shield === "pass" ? "green" : "red"
              return (
                <div>
                  <img src={"https://img.shields.io/badge/" + item.name +"-" + item.shield + "-" + color }></img><a href={item.how_to_help}>Comment aider ?</a>
                </div>
              )
            }
          )
        }

      Encore mieux : récupérer la liste des répos ouverts sur gitlab et les badges associés
        

      </div>

      <div style={{ marginLeft: "25%", marginRight: "25%", fontSize: "1.2em", textAlign: "center" }}>
        <Md
          md="Impliquez massivement les sympathisant·e·s de votre projet
          "
        />
        <Md
          md=""
        />
      </div>

      {/*         <Media query={{ maxWidth: 599 }}>
          {matches =>
            matches ? 
                    : <div style={{ textAlign: "center" }}> 
          } 
 */}

      <div style={{ display: useMediaPredicate("(min-width: 400px)") ? "grid" : null, gridTemplateColumns: "1fr 1fr", textAlign: "center" }}>
        <div>
          <Md md="Community Organizing"
          />
          <Md md="..." />
          <Md md="#Contribution" />
          <p>Missions</p>

          <iframe title="manifeste-community-organizing" src="https://codimd.communecter.org/s/wXMxIVP0H"></iframe>
          <iframe title="missions" src="https://mic-mac.gitlab.io"></iframe>
        </div>
        <div>

          <Md md="Concierge des orgas de transition"/>
          <Md md="relai entre pairs, favorisant coopération, questionnaires & données" />
          <Md md="#Décentralisation" />
          <p>média participatif</p>
          <iframe title="manifeste-concierge" src="https://codimd.communecter.org/s/0wEykUyNU"></iframe>
          <iframe title="airtable" class="airtable-embed airtable-dynamic-height" src="https://nbviewer.jupyter.org/urls/gitlab.com/jibe-b/sondages/-/raw/master/index.ipynb" frameborder="0" onmousewheel="" width="100%" height="221" style={{background: "transparent", border: "1px solid #ccc"}}></iframe>
        </div>

      </div>

      <div style={{ marginLeft: "25%", marginRight: "25%", fontSize: "1.2em", textAlign: "center" }}>
        <Md md="Je le fais en étudiant le niveau de décentralisation dans une communauté et en évaluant la capacité à encaisser une variation brusque des conditions extérieures (crise)" />
      </div>

      <div style={{ marginLeft: "25%", marginRight: "25%", fontSize: "1em", textAlign: "center" }}>

      <iframe title="analyses" src="https://nbviewer.jupyter.org/urls/gitlab.com/jibe-b/analyses/-/raw/master/index.ipynb"></iframe>
        <p>
          Accéder à un environnement d'exécution des analyses
        </p>
        <a href="ttps://mybinder.org/v2/gl/jibe-b%2Fanalyses/master?urlpath=lab%2Ftree%2Findex.ipynb" ><img alt="Binder" src="https://mybinder.org/badge_logo.svg" ></img></a>
      </div>

      <br />
      <br />
      <br />

      <div style={{ marginLeft: "25%", marginRight: "25%", fontSize: "1.2em", textAlign: "center" }}>
        <Md md="*Hey les porteurs du projet…*" />
      </div>


      <div style={{ display: useMediaPredicate("(min-width: 400px)") ? "grid" : null, gridTemplateColumns: "1fr 1fr", textAlign: "center" }}>

        <div>
          <Md md="…regardez ce que votre communauté a collecté et va continuer collecter de manière autonome." />
          <Md md="**Écoutez la communauté et faites-leur confiance en partageant la gouvernance avec elles et eux !**" />
        </div>

        <div>
          <Md md="…on a commencé mais on n'est pas expert·e·s, aidez-nous à bien nous y prendre !" />
          <Md md="**On va garder notre indépendance, mais on va faire savoir que vous avez contribué en nous aidant !**" />
        </div>




      </div>
      <div style={{ marginLeft: "25%", marginRight: "25%", fontSize: "1.2em", textAlign: "center" }}>
        <Md md="Et regardez, c'est déjà bien en route !" />
        <p>Pulse</p>
        <iframe title="pulse" src="https://mic-mac.gitlab.io"></iframe>

        <br />
        <button>
          <Md md="Je me lance !" />
        </button>
      </div>

      <div>
        <p>
          Vous êtes un porteur de projet ? Consultez la liste de contributions auxquelles vous pourrez avoir droit si vous participez à la dynamique
        </p>
      </div>


    </Layout>
  )
}








/*
import Layout from "../components/layout"
import SEO from "../components/seo"
import Constats from "../components/constats"
import SuggestionActions from "../components/suggestion-actions"
import AppelAParticipers from "../components/appel-a-participers"
import ActionEnCourss from "../components/action-en-courss"
import Offres from "../components/offres"

import Constat from "../components/constat"
import SuggestionAction from "../components/suggestion-action"
import AppelAParticiper from "../components/appel-a-participer"
import ActionEnCours from "../components/action-en-cours"
import Offre from "../components/offre"
 */
/*
export default () => (
  <Layout>
    <SEO title="Home" />
    <h1>Jibé B.</h1>
    <p>Un contributeur et activateur de contribution.</p>
    <Constats>
      <Constat>Des gens meurent à petit feu à la rue.</Constat>
      <Constat>Ce que nous rejetons dans l'environnement accélère une extermination de masse et est parti pour empêcher la vie sur terre.</Constat>
      <Constat>Des gens ont accès à l'argent que l'on mutualise via l'impôt et l'utilisent pour leurs intérêts.</Constat>
    </Constats>

    <SuggestionActions>
      <SuggestionAction>Reprenons l'économie. Je vous propose de commencer par une action qui ne vous coûtera rien.</SuggestionAction>
      <SuggestionAction>Reprenons ce qui se fait dans notre ville. Je vous propose de commencer par les actions concrètes pour l'écologie, la solidarité et l'instruction des dossiers des votre ville.</SuggestionAction>
    </SuggestionActions>

    <AppelAParticipers>
      <AppelAParticiper>
        <Link to="mic-mac">Il y a déjà plein de gens qui agissent ! Mettons-nous massivement à agir !</Link>
      </AppelAParticiper>
    </AppelAParticipers>

    <ActionEnCourss>
      <ActionEnCours>Développement de la communauté de contribution de la coopérative de covoiturage sans commission Mobicoop.</ActionEnCours>
      <ActionEnCours>Développement et animation de forums de contribution à Strasbourg et Heidelberg.</ActionEnCours>
    </ActionEnCourss>

    <Offres>
      <Offre>J'offre des prestations de conseil en développement de la communauté de contribution autour de votre projet </Offre>
      <Offre>Je propose des formations d'inituation à l'analyse de données et au développement web.</Offre>
    ..<Offre>Je vous propose mon aide gratuitement sur votre projet, sur un sujet sur lequel je suis en train d'apprendre. Sans engagement de moyens ou de résultat par conséquent, mais à plusieurs on a plus de chances d'aboutir. Consultez la <a src="learning/">liste des sujets sur lesquels je souhaite apprendr</a>.</Offre>
    </Offres>

    <h3>Je m'exprime sur les réseaux sociaux</h3>
    <div><a href="https://facebook.com/jibe.bohuon">Mon compte facebook</a></div>
    <div><a href="https://twitter.com/jibe_jeybee">Mon compte twitter</a></div>


    <div>
        <h3>Projets de développement de la contribution à Strasbourg</h3>
        <iframe src="https://mic-mac.gitlab.io/" width="25%" height="200px" />
        <iframe src="https://contribution.costrasbourg.gitlab.io/" width="25%" height="200px" />
        <iframe src="https://instruction-citoyenne-strasbourg.gitlab.io/" width="25%" height="200px" />
        <iframe src="https://universite-pair-a-pair-strasbourg.gitlab.io/" width="25%" height="200px" />
    </div>

    <div>
        <h3>Projets de développement de la contribution partout</h3>
        <iframe src="https://contribution-coop.gitlab.io/" width="25%" height="200px" />
        <iframe src="https://contribution.mobicoop.gitlab.io/" width="25%" height="200px" />
        <iframe src="apps.costrasbourg.gitlab.io" width="25%" height="200px" />
        <iframe src="apps-perimees.costrasbourg.gitlab.io" width="25%" height="200px" />
    </div>

    <div>
      Envie d'agir concrètement ? <a href="https://mic-mac.gitlab.io">Rendez-vous sur Mic-Mac !</a>
    </div>

    <div>
    <div style={{textAlign: "center"}}>
      <p>Rendez-vous sur <a href="https://mic-mac.gitlab.io">mic-mac.gitlab.io</a> pour installer l'appli !</p>
      <p>Voici déjà un aperçu de l'appli :</p>
    </div>
    <div>
      <iframe title="mic-mac" width="100%" height="900px" src="https://mic-mac.gitlab.io" style={{paddingLeft: "0%"}} />
    </div>
    <div>
      <p style={{textAlign: "center"}}>En participant à la communauté, vous aidez à faire avancer les projets !</p>
    </div>
  </div>
  </Layout>
)
 */
