import React from "react"

//import { Fragment } from 'react';

import Md from "../components/basic_html_from_markdown"

//import Media from "react-media"
import { useMediaPredicate } from "react-media-hook";

import Layout from "../components/layout"

export default () => {

  1 < 2 && console.log(true)
  /* 
  {
    (1 < 2 && "A") || "B"
  }
   */

  return (
    <Layout>
      {/* 
      <div>
        <Media queries={{
          small: "(max-width: 599px)",
          medium: "(min-width: 600px) and (max-width: 1199px)",
          large: "(min-width: 1200px)"
        }}>
          {matches => (
            <Fragment>
              {matches.small && <p>I am small!</p>}
              {matches.medium && <p>I am medium!</p>}
              {matches.large && <p>I am large!</p>}
            </Fragment>
          )}
        </Media>
      </div>
*/}

      <div style={{ marginLeft: "25%", marginRight: "25%", fontSize: "1.2em", textAlign: "center" }}>
        <Md
          md="**Désobéir**, c'est votre truc ?
              La désobéissance civique, ça vous tente ?
              Ou au contraire ce n'est pas pour vous,  mais ça vous parle ?"
        />
      </div>

      {/*         <Media query={{ maxWidth: 599 }}>
          {matches =>
            matches ? 
                    : <div style={{ textAlign: "center" }}> 
          } 
 */}

      <div style={{ display: useMediaPredicate("(min-width: 400px)") ? "grid" : null, gridTemplateColumns: "1fr 1fr", textAlign: "center" }}>
        <div>
          <Md md="Relevons les trucs qui clochent que nous repérons, dès qu'on les repère !
              Montrons ainsi ce qui est important pour les usager·e·s et qui échape aux porteurs de projets."
          />
          <Md md="**N'attendons pas qu'on nous en donne le droit pour contribuer**" />
          <Md md="#Contribution" />
          <p>Y a un truc qui cloche</p>
          <iframe title="yauntrucquicloche" src="https://mic-mac.gitlab.io"></iframe>
        </div>
        <div>

          <Md md="Réalisons des actions concrètes en suivant des tutoriels, sans attendre un encadrement,
              et communiquons entre nous pour nous coordonner"/>
          <Md md="**Adoptons une architecture décentralisée, décidons par nous-mêmes**" />
          <Md md="#Décentralisation" />
          <p>tutos</p>
          <iframe title="tutos" src="https://mic-mac.gitlab.io"></iframe>
        </div>

      </div>

      <div style={{ marginLeft: "25%", marginRight: "25%", fontSize: "1.2em", textAlign: "center" }}>
        <Md md="Lançons-nous sans attendre, certes, mais pas ne restons pas isolé·e·s ! Communiquons entre nous !" />
        <iframe title="chat" src="https://chat.communecter.org"></iframe>
        <Md md="Et je souhaite que nous interagissions avec les porteurs de projet, comme ceci :" />
        <Md md="*Hey les porteurs du projet…*" />
      </div>


      <div style={{ display: useMediaPredicate("(min-width: 400px)") ? "grid" : null, gridTemplateColumns: "1fr 1fr", textAlign: "center" }}>

        <div>
          <Md md="…regard ez ce que votre communauté a collecté et va continuer collecter de manière autonome." />
          <Md md="**Écoutez la communauté et faites-leur confiance en partageant la gouvernance avec elles et eux !**" />
        </div>

        <div>
          <Md md="…on a commencé mais on n'est pas expert·e·s, aidez-nous à bien nous y prendre !" />
          <Md md="**On va garder notre indépendance, mais on va faire savoir que vous avez contribué en nous aidant !**" />
        </div>




      </div>
      <div style={{ marginLeft: "25%", marginRight: "25%", fontSize: "1.2em", textAlign: "center" }}>
        <Md md="Et regardez, c'est déjà bien en route !" />
        <p>Pulse</p>
        <iframe title="pulse" src="https://mic-mac.gitlab.io"></iframe>

        <br />
        <button>
          <Md md="Je me lance !" />
        </button>
      </div>

      <div>
        <p>
          Vous êtes un porteur de projet ? Consultez la liste de contributions auxquelles vous pourrez avoir droit si vous participez à la dynamique
        </p>
      </div>


    </Layout>
  )
}








/*
import Layout from "../components/layout"
import SEO from "../components/seo"
import Constats from "../components/constats"
import SuggestionActions from "../components/suggestion-actions"
import AppelAParticipers from "../components/appel-a-participers"
import ActionEnCourss from "../components/action-en-courss"
import Offres from "../components/offres"

import Constat from "../components/constat"
import SuggestionAction from "../components/suggestion-action"
import AppelAParticiper from "../components/appel-a-participer"
import ActionEnCours from "../components/action-en-cours"
import Offre from "../components/offre"
 */
/*
export default () => (
  <Layout>
    <SEO title="Home" />
    <h1>Jibé B.</h1>
    <p>Un contributeur et activateur de contribution.</p>
    <Constats>
      <Constat>Des gens meurent à petit feu à la rue.</Constat>
      <Constat>Ce que nous rejetons dans l'environnement accélère une extermination de masse et est parti pour empêcher la vie sur terre.</Constat>
      <Constat>Des gens ont accès à l'argent que l'on mutualise via l'impôt et l'utilisent pour leurs intérêts.</Constat>
    </Constats>

    <SuggestionActions>
      <SuggestionAction>Reprenons l'économie. Je vous propose de commencer par une action qui ne vous coûtera rien.</SuggestionAction>
      <SuggestionAction>Reprenons ce qui se fait dans notre ville. Je vous propose de commencer par les actions concrètes pour l'écologie, la solidarité et l'instruction des dossiers des votre ville.</SuggestionAction>
    </SuggestionActions>

    <AppelAParticipers>
      <AppelAParticiper>
        <Link to="mic-mac">Il y a déjà plein de gens qui agissent ! Mettons-nous massivement à agir !</Link>
      </AppelAParticiper>
    </AppelAParticipers>

    <ActionEnCourss>
      <ActionEnCours>Développement de la communauté de contribution de la coopérative de covoiturage sans commission Mobicoop.</ActionEnCours>
      <ActionEnCours>Développement et animation de forums de contribution à Strasbourg et Heidelberg.</ActionEnCours>
    </ActionEnCourss>

    <Offres>
      <Offre>J'offre des prestations de conseil en développement de la communauté de contribution autour de votre projet </Offre>
      <Offre>Je propose des formations d'inituation à l'analyse de données et au développement web.</Offre>
    ..<Offre>Je vous propose mon aide gratuitement sur votre projet, sur un sujet sur lequel je suis en train d'apprendre. Sans engagement de moyens ou de résultat par conséquent, mais à plusieurs on a plus de chances d'aboutir. Consultez la <a src="learning/">liste des sujets sur lesquels je souhaite apprendr</a>.</Offre>
    </Offres>

    <h3>Je m'exprime sur les réseaux sociaux</h3>
    <div><a href="https://facebook.com/jibe.bohuon">Mon compte facebook</a></div>
    <div><a href="https://twitter.com/jibe_jeybee">Mon compte twitter</a></div>


    <div>
        <h3>Projets de développement de la contribution à Strasbourg</h3>
        <iframe src="https://mic-mac.gitlab.io/" width="25%" height="200px" />
        <iframe src="https://contribution.costrasbourg.gitlab.io/" width="25%" height="200px" />
        <iframe src="https://instruction-citoyenne-strasbourg.gitlab.io/" width="25%" height="200px" />
        <iframe src="https://universite-pair-a-pair-strasbourg.gitlab.io/" width="25%" height="200px" />
    </div>

    <div>
        <h3>Projets de développement de la contribution partout</h3>
        <iframe src="https://contribution-coop.gitlab.io/" width="25%" height="200px" />
        <iframe src="https://contribution.mobicoop.gitlab.io/" width="25%" height="200px" />
        <iframe src="apps.costrasbourg.gitlab.io" width="25%" height="200px" />
        <iframe src="apps-perimees.costrasbourg.gitlab.io" width="25%" height="200px" />
    </div>

    <div>
      Envie d'agir concrètement ? <a href="https://mic-mac.gitlab.io">Rendez-vous sur Mic-Mac !</a>
    </div>

    <div>
    <div style={{textAlign: "center"}}>
      <p>Rendez-vous sur <a href="https://mic-mac.gitlab.io">mic-mac.gitlab.io</a> pour installer l'appli !</p>
      <p>Voici déjà un aperçu de l'appli :</p>
    </div>
    <div>
      <iframe title="mic-mac" width="100%" height="900px" src="https://mic-mac.gitlab.io" style={{paddingLeft: "0%"}} />
    </div>
    <div>
      <p style={{textAlign: "center"}}>En participant à la communauté, vous aidez à faire avancer les projets !</p>
    </div>
  </div>
  </Layout>
)
 */
