import React from "react"

export default () => (
  <div>
    <div style={{textAlign: "center"}}>
      <p>Rendez-vous sur <a href="https://mic-mac.gitlab.io">mic-mac.gitlab.io</a> pour installer l'appli !</p>
      <p>Voici déjà un aperçu de l'appli :</p>
    </div>
    <div>    
      <iframe title="mic-mac" width="100%" height="900px" src="https://mic-mac.gitlab.io" style={{paddingLeft: "0%"}} />  
    </div>
    <div>
      <p style={{textAlign: "center"}}>En participant à la communauté, vous aidez à faire avancer les projets !</p>
    </div>
  </div>
)