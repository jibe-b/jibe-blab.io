import React from "react"

export default props => (
  <div>
    <h3>Je suggère ces actions</h3>
    <table style={{tableLayout: "fixed"}}>
      <tr>
        {props.children}
      </tr>
    </table>
  </div>
)
