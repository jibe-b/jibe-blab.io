import React from "react"

export default props => (
  <div>
    <h3>Mes actions en cours</h3>
    <table style={{tableLayout: "fixed"}}>
      <tr>
        {props.children}
      </tr>
    </table>
  </div>
)
