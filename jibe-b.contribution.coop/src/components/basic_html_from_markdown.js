import React from "react"
import marked from 'marked';

export default props => {
    const getMarked = text_in_markdown =>{
        //const text_in_markdown_array = typeof(text_in_markdown) === typeof([]) ? text_in_markdown : new Array(text_in_markdown)

        //const text_in_html = text_in_markdown_array.map(item => marked(item))
        const text_in_html = marked(text_in_markdown)
        return { __html: text_in_html}
      }
      return(
            <div dangerouslySetInnerHTML={getMarked(props.md)} ></div>
      )
}