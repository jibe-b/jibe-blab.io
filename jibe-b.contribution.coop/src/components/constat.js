import React from "react"

export default props => (
  <td>{props.children}</td>
)
