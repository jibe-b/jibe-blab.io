module.exports = {
  siteMetadata: {
    title: `Jibé B @ Contribution.coop`,
    description: `Kick off your next, great Gatsby project with this default starter. This barebones starter ships with the main Gatsby configuration files you might need.`,
    author: `jibe-b`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
/*    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `contribution.coop`,
        short_name: `contribution.coop`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`
      },
    },
    `gatsby-plugin-offline`,
*/
  ],
}
